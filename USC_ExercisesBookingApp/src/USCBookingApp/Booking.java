/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USCBookingApp;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Baratharun
 */
public class Booking {
    Scanner input = new Scanner(System.in);
    private int bookingID , exerciseWeek, exerciseDay, exerciseSession, exerciseID, maxBooking, rating;
    private String bookingStatus, studentID , studentName,reviewMessage;
    private static ArrayList <Booking> bookingList = new ArrayList <Booking>();
    
    
    
    public Booking(){
        
    }
    
    public Booking(int bookingID, int exerciseWeek, int exerciseDay, int exerciseSession, int exerciseID ,String studentID, String studentName, int maxBooking, String bookingStatus, int rating, String rMessage){
        this.bookingID = bookingID;
        this.exerciseWeek = exerciseWeek;
        this.exerciseDay = exerciseDay;
        this.exerciseID = exerciseID;
        this.exerciseSession = exerciseID;
        this.bookingStatus = bookingStatus;
        this.studentID = studentID;
        this.studentName= studentName;
        this.maxBooking = maxBooking;
        this.rating = rating;
        this.reviewMessage = rMessage;
    }
    
    public void createBooking(int bookingID, int exerciseWeek, int exerciseDay, int exerciseSession, int exerciseID ,String studentID,String studentName,int maxBooking, String bookingStatus,int rating, String rMessage)
    {
        Booking bookingObj = new Booking(bookingList.size()+1, exerciseWeek,exerciseDay,exerciseSession, exerciseID , studentID, studentName, maxBooking, bookingStatus, rating, rMessage);
        bookingList.add(bookingList.size(),bookingObj);
       
    }
    
    
    
//data retrieved from list with called booking ID and the new parameters chosen by user.   
    public void changeBookings(int bID, int exWeek,int exDay,int exSess,int exID,String sID,String sName,int maxBook, String bStatus, int rating, String rMessage)
    {
        ArrayList <Booking> bookingList = this.getBookingList();
        
        bookingList.get(bID-1).setExerciseWeek(exWeek);
        bookingList.get(bID-1).setExerciseDay(exDay);
        bookingList.get(bID-1).setExerciseSession(exSess);
        bookingList.get(bID-1).setExerciseID(exID);
        sID = bookingList.get(bID-1).getStudentID();
        sName = bookingList.get(bID-1).getStudentName();
        maxBook = bookingList.get(bID-1).getMaxBooking();
        rating = bookingList.get(bID-1).getRating();
//        rMessage=bookingList.get(bID).getReviewMessage();
        
        this.updateBookings(bID,exWeek,exDay,exSess,exID,sID,sName,maxBook,bStatus,rating,rMessage);
    }

//data retrieved from list with called booking ID and status.    
    public void cancelBookings(int bID,String bStatus)
    {     
        
        ArrayList <Booking> bookingList = this.getBookingList();
        
        int exWeek = bookingList.get(bID-1).getExerciseWeek();
        int exDay = bookingList.get(bID-1).getExerciseDay();
        int exSess = bookingList.get(bID-1).getExerciseSession();
        int exID = bookingList.get(bID-1).getExerciseID();
        String sID = bookingList.get(bID-1).getStudentID();
        String sName = bookingList.get(bID-1).getStudentName();
        int maxBook = bookingList.get(bID-1).getMaxBooking();
        int rating = bookingList.get(bID-1).getRating();
        String rMessage=bookingList.get(bID-1).getReviewMessage();
        
        this.updateBookings(bID,exWeek,exDay,exSess,exID,sID,sName,maxBook,bStatus,rating,rMessage);  
    }
    
//  a new booking object created and replaced in place with the existing booking. Incrementing the slot by one.  
    public void updateBookings(int bookingID, int exerciseWeek, int exerciseDay, int exerciseSession, int exerciseID ,String studentID,String studentName,int maxBooking, String bookingStatus, int rating, String rMessage)
    {  
        Booking modBook = new Booking(bookingID,exerciseWeek,exerciseDay,exerciseSession,exerciseID,studentID,studentName,maxBooking-1,bookingStatus,rating,rMessage);
        bookingList.set(bookingID-1,modBook);
    }
    
// attend the booking and insert the review given by the user.      
    public void attendBookings(int bID,String bStatus, int rating,String rMessage)
    {
        ArrayList <Booking> bookingList = this.getBookingList();
       
        bookingList.get(bID-1).setBookingStatus(bStatus);
        bookingList.get(bID-1).setRating(rating);
        bookingList.get(bID-1).setReviewMessage(rMessage);
    }
    
    // checks if the booking list is empty
    public boolean emptyBookingList(){
        
        ArrayList <Booking> bookingList = this.getBookingList();
        boolean empty = bookingList.isEmpty();
        if(empty)
        {
            return true;
        }
        return false;
    }
    /**
     * @return the bookingID
     */
    public int getBookingID() {
        return bookingID;
    }

    /**
     * @param bookingID the bookingID to set
     */
    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    /**
     * @return the exerciseDay
     */
    public int getExerciseDay() {
        return exerciseDay;
    }

    /**
     * @param exerciseDay the exerciseDay to set
     */
    public void setExerciseDay(int exerciseDay) {
        this.exerciseDay = exerciseDay;
    }

    /**
     * @return the exerciseSession
     */
    public int getExerciseSession() {
        return exerciseSession;
    }

    /**
     * @param exerciseSession the exerciseSession to set
     */
    public void setExerciseSession(int exerciseSession) {
        this.exerciseSession = exerciseSession;
    }

    /**
     * @return the exerciseID
     */
    public int getExerciseID() {
        return exerciseID;
    }

    /**
     * @param exerciseID the exerciseID to set
     */
    public void setExerciseID(int exerciseID) {
        this.exerciseID = exerciseID;
    }

    /**
     * @return the bookingStatus
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * @param bookingStatus the bookingStatus to set
     */
    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    /**
     * @return the bookingList
     */
    public ArrayList <Booking> getBookingList() {
        return bookingList;
    }

    /**
     * @param bookingList the bookingList to set
     */
    public void setBookingList(ArrayList <Booking> bookingList) {
        this.bookingList = bookingList;
    }

    /**
     * @return the studentID
     */
    public String getStudentID() {
        return studentID;
    }

    /**
     * @param studentID the studentID to set
     */
    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    /**
     * @return the maxBooking
     */
    public int getMaxBooking() {
        return maxBooking;
    }

    /**
     * @param maxBooking the maxBooking to set
     */
    public void setMaxBooking(int maxBooking) {
        this.maxBooking = maxBooking;
    }

    /**
     * @return the studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @param studentName the studentName to set
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    /**
     * @return the exerciseWeek
     */
    public int getExerciseWeek() {
        return exerciseWeek;
    }

    /**
     * @param exerciseWeek the exerciseWeek to set
     */
    public void setExerciseWeek(int exerciseWeek) {
        this.exerciseWeek = exerciseWeek;
    }

    /**
     * @return the rating
     */
    public int getRating() {
        return rating;
    }

    /**
     * @param rating the rating to set
     */
    public void setRating(int rating) {
        this.rating = rating;
    }

    /**
     * @return the reviewMessage
     */
    public String getReviewMessage() {
        return reviewMessage;
    }

    /**
     * @param reviewMessage the reviewMessage to set
     */
    public void setReviewMessage(String reviewMessage) {
        this.reviewMessage = reviewMessage;
    }
    
    public void listAllBookings()
    {   
        
            for (int i = 0; i < bookingList.size(); i++)
            {
            System.out.println("======================================================================================================================================================================");
                System.out.println(i +1+ " : Booking ID: " + 
                       (bookingList.get(i).getBookingID()) +" | "+
                    "Exercise ID: "+(bookingList.get(i).getExerciseID())
                +" | Student name: "+(bookingList.get(i).getStudentName().toUpperCase())
                +" | Booking status: "+ (bookingList.get(i).getBookingStatus())
                +" | Rating: ["+(bookingList.get(i)).getRating()+"]"
                +" | Review: '"+(bookingList.get(i).getReviewMessage())+"'");
            }
            System.out.println("======================================================================================================================================================================");
        
    }
    
    
    
}
