/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USCBookingApp;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Baratharun
 */
public class RunApplication {
    Scanner input = new Scanner(System.in);
    
    private static ArrayList<Exercises> exList = new ArrayList<Exercises>();
    private ArrayList <Student> stuDetails = new ArrayList <Student>();
    
    private static final String[] SESSIONS = new String[3]; 
    private static final String[] DAYS = new String[2];
    private static final String[] WEEKS = {"","Week-1","Week-2","Week-3","Week-4","Week-5","Week-6","Week-7","Week-8"};
    private static final String BOOKED="Booked",CANCELLED="Cancelled",ATTENDED="Attended", CHANGED="Changed", NOT_REVIEWED = "Not rated yet";
    private static final int VERY_DISSATISFIED= 1, DISSATISFIED= 2, OK = 3, SATISFIED = 4,VERY_SATISFIED = 5, DISABLED =0;
    private static final int BOOK_CLASS =1, CANCEL_BOOKING=2, CHANGE_BOOKING = 3,ATTEND_CLASS_REVIEW= 4, VIEW_CLASS=5, VIEW_TIMETABLE =6, PRINT_REPORT =7,PRINT_C_REPORT =8, EXIT=9; 
    static final int MAX_STUDENTS = 4;
    
    
    
    
    Student student = new Student();
    Booking booking = new Booking();
    
    public RunApplication()
    {
        SESSIONS[0]="Morning";
        SESSIONS[1]="Afternoon";
        SESSIONS[2]="Evening";
        
        DAYS[0]="Saturday";
        DAYS[1]="Sunday";
        
        setExercises();
        navigationPane();
    }
    
     public static void main(String abc[])
    {
        
        RunApplication run = new RunApplication();
        run.navigationPane(); 
    }
    
    public void setExercises(){
        Exercises exercise1 = new Exercises("Yoga    ",5.99,0);
        Exercises exercise2 = new Exercises("Boxing  ",9.99,1);
        Exercises exercise3 = new Exercises("Crossfit",7.99,2);
        Exercises exercise4 = new Exercises("Zumba   ",4.99,3);
        
        exList.add(0, exercise1);
        exList.add(1, exercise2);
        exList.add(2, exercise3);
        exList.add(3, exercise4);
        
    }

    public ArrayList<Exercises> getExercises(){

        return exList;
    }
    
    public void listAllExercise(int week)
    {   
        System.out.println("    UNIVERSITY SPORTS CENTRE EXERCISE CLASSES");
        System.out.printf("|WEEK %45S|",week);
        System.out.println("");
        for (int i = 0; i < exList.size(); i++){
            printTableBorder();
            System.out.println("Exercise ID: " + 
                       (exList.get(i).getExerciseID()) +" || Name: "+ (exList.get(i).getExerciseName().toUpperCase())+
                    " || Price: £"+ (exList.get(i).getExercisePrice()));
            }
        printTableBorder();
        
    }
    
    public void navigationPane(){
        
        int choice=0;
        do{
        System.out.println("~~~~~~~ WELCOME TO USC BOOKING SYSTEM ~~~~~~~" +
                "\n\n1]\tBook an excercise class \n2]\tCancel a booking"
                +"\n3]\tChange a booking"
                + "\n4]\tAttend a class & give reviews \n5]\tView my bookings \n6]\tView time table"
                + "\n7]\tPrint report \n8]\tPrint champion report \n9]\tExit\n"
                + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.print("Please select a choice between 1 - 9 : ");
        try{
            choice = input.nextInt();
        } catch (Exception e){
            System.out.println("Invalid input \nPlease try again");
            RunApplication run = new RunApplication();
            run.navigationPane();
            break; 
        }
    }while(choice <=0 || choice>9);
    
    
    switch(choice){
        case BOOK_CLASS:
            bookingSlots();
            navigationPane();
            break;
            
        case CANCEL_BOOKING:
            printDivider();
            if(booking.emptyBookingList())
            {
                System.out.println("No records found");
            }
            else
            {
                viewBooking(student.getStudentID());
                System.out.println("Please enter Bookind ID");
                int bookingID = input.nextInt();
                if(authenticateBookingId(authStudentID(student.getStudentID()),bookingID))
                {
                    if(authCancelStatus(bookingID))
                    {
                        booking.cancelBookings(bookingID,CANCELLED);
                        printHeader("You have successfully cancelled the booking now.");
                    }
                    else
                    {
                        printAlertMessage("Unsucessful! \nEither this booking is 'Cancelled' or 'Attended' already.");
                    }
                }
                else
                {
                    printAlertMessage("Booking ID does not associate with the Student ID");
                }
            }
            printDivider();
            navigationPane();
            break;

        case CHANGE_BOOKING:
            printDivider();
            if(booking.emptyBookingList())
            {
                System.out.println("No records found");
            }
            else
            {
                viewBooking(student.getStudentID());
                System.out.println("Please enter Bookind ID");
                int booID = input.nextInt();
                
                if(authenticateBookingId(authStudentID(student.getStudentID()),booID))
                {
                    if(authChangeStatus(booID))
                    {
                        modifiedBookingSlot(booID, CHANGED);
                        printHeader("You have successfully changed the booking now.");
                    }
                    else
                    {
                        printAlertMessage("Unsucessful! \nEither this booking is 'Changed' / Cancelled' / 'Attended' already.");
                    }
                }
                else
                {
                    printAlertMessage("Booking ID does not associate with the Student ID");
                }
            }
            navigationPane();
            break;

        case ATTEND_CLASS_REVIEW:
            printDivider();
            if(booking.emptyBookingList())
            {
                System.out.println("No records found");
            }
            else
            {
                System.out.println("ATTEND A CLASS & GIVE FEEDBACKS");
                printTableBorder();
                viewBooking(student.getStudentID());
                System.out.println("Please enter Bookind ID");
                int bookID = input.nextInt();
                if(authenticateBookingId(authStudentID(student.getStudentID()),bookID))
                {
                    if(authAttendanceStatus(bookID))
                    {
                        do{
                        printDivider();
                        System.out.println("(1: Very dissatisfied, 2: Dissatisfied, 3: Ok, 4: Satisfied, 5: Very Satisfied)");
                        System.out.println("");
                        System.out.print("Please provide a numerical rating from 1 - 5 : ");
                        try
                        {
                            choice = input.nextInt();
                        } catch (Exception e){
                            System.out.println("Invalid input \nPlease try again");
                            //choice = input.nextInt();
                            break; 
                            }
                        }while(choice <=0 || choice>5);
                        input.nextLine();
                        System.out.print("Please give a review of no more than 20 characters long: ");
                        String rMessage = input.nextLine();

                        switch(choice){
                            case 1 :
                                userReview(rMessage);
                                booking.attendBookings(bookID,ATTENDED,VERY_DISSATISFIED,rMessage);
                            break;
                            case 2 :
                                userReview(rMessage);
                                booking.attendBookings(bookID,ATTENDED,DISSATISFIED,rMessage);
                            break;
                            case 3 :
                                userReview(rMessage);
                                booking.attendBookings(bookID,ATTENDED,OK,rMessage);
                            break;
                            case 4 :
                                userReview(rMessage);
                                booking.attendBookings(bookID,ATTENDED,SATISFIED,rMessage);
                            break;
                            case 5 :
                                userReview(rMessage);
                                booking.attendBookings(bookID,ATTENDED,VERY_SATISFIED,rMessage);

                            break;
                        }
                        printHeader("Atendance and Feedback has been processed successfully");
                    }

                    else
                    {
                        printAlertMessage("Unsucessful! \nEither this booking is 'Cancelled' or 'Attended' already.");
                    }
                }
                else
                {
                    printAlertMessage("Booking ID does not associate with the Student ID");
                }
                
            }
            navigationPane();
            break;
            
        case VIEW_CLASS:
            printDivider();
            if(booking.emptyBookingList())
            {
                printAlertMessage("No booking records found");
            }
            else
            {
                viewBooking(student.getStudentID());
            }
            navigationPane();
            break;
        
        case VIEW_TIMETABLE:
            printDivider();
            System.out.println("    UNIVERSITY SPORTS CENTRE EXERCISE CLASSES");
            for (int i = 0; i < exList.size(); i++){
            printTableBorder();
            System.out.println("Exercise ID: " + 
                       (exList.get(i).getExerciseID()) +" || Name: "+ (exList.get(i).getExerciseName().toUpperCase())+
                    " || Price: £"+ (exList.get(i).getExercisePrice()));
            }
            printDivider();
            
            break;
            
        case PRINT_REPORT:
            System.out.println("PRINT REPORT");
            printTableBorder();
            System.out.println("1. 1ST MONTH | 2. 2ND MONTH");
            System.out.println("");
            int weekend =0;
            do{
                System.out.print("Please select a choice (1/2): ");
                try
                {
                    weekend  = input.nextInt();
                }
                catch (Exception e)
                {
                    System.out.println("Invalid input \nPlease try again");
                    break; 
                }
            }while(weekend  <=0 || weekend >2);
            
            switch(weekend){
                case 1:
                   printTableBorder();
                   firstMonthlyReport();
                   printDivider();
                   break;
                case 2:
                   printTableBorder();
                   secondMonthlyReport();
                   printDivider();
                   break;
                }
            
            navigationPane();
            break;
        
        case PRINT_C_REPORT:
            System.out.println("PRINT REPORT");
            printTableBorder();
            System.out.println("1. 1ST MONTH | 2. 2ND MONTH");
            System.out.println("");
            int monthChoice =0;
            do{
                System.out.print("Please select a choice (1/2): ");
                try
                {
                    monthChoice  = input.nextInt();
                }
                catch (Exception e)
                {
                    System.out.println("Invalid input \nPlease try again");
                    break; 
                }
            }while(monthChoice  <=0 || monthChoice >2);
            
            if(monthChoice == 1)
            {
               printChampionReport(monthChoice);
               navigationPane();
            }
            else if(monthChoice == 2)
            {
               printChampionReport(monthChoice);
               navigationPane();
            }
            break;
            
        case EXIT:
            printHeader("Thank you for visiting us. Good bye!");
            System.exit(0);
            break;
            
        }
    
    }
    
    
    
    public String viewBooking(String stuID){
        boolean validV = true;
            while (validV){
                System.out.print("Please enter the 2 digits student ID number: ");
                input.nextLine();
                stuID = input.nextLine();
                char[] ch = stuID.toCharArray();

                if (ch.length<=1 || ch.length>=3){
                System.out.println("Invalid ID number please try again");
                validV = true;
                }
                else
                {
                    validV = false;
                }
            }
        
        ArrayList<Booking> bookingList = booking.getBookingList();
        
        for(Booking b: bookingList)
        {
            String studentID = b.getStudentID();
            int exSession = b.getExerciseSession();
            int exID = b.getExerciseID();
            int exDay = b.getExerciseDay();
            int exWeek = b.getExerciseWeek();
            String eName = exList.get(exID).getExerciseName();
            String sName = b.getStudentName();
            
            if(studentID.equalsIgnoreCase(stuID)&& !studentID.isEmpty())
            {   
                printTableBorder();
                
                System.out.println("Booking ID : \t\t\t["+b.getBookingID()+"]"
                                        + "\nStudent name : \t\t\t" + sName
                                        + "\nExercise class:\t\t\t"+ eName
                                        + "\nDay: \t\t\t\t"+DAYS[exDay]
                                        + "\nWeek: \t\t\t\t"+WEEKS[exWeek]
                                        + "\nBooking status:\t\t\t"+b.getBookingStatus()
                                        + "\nRating:\t\t\t\t"+b.getRating()
                                        + "\nReview:\t\t\t\t"+b.getReviewMessage());                
                printTableBorder();
                
            }
            
        }
        return stuID;
    }
    
    public String authStudentID(String stuID){
        boolean validV = true;
            while (validV){
                System.out.print("Re-confirm your 2 digits student ID number: ");
                input.nextLine();
                stuID = input.nextLine();
                char[] ch = stuID.toCharArray();

                if (ch.length<=1 || ch.length>=3){
                System.out.println("Invalid ID number please try again");
                validV = true;
                }
                else
                {
                    validV = false;
                }
            }
        
        ArrayList<Booking> bookingList = booking.getBookingList();
        
        for(Booking b: bookingList)
        {
            String studentID = b.getStudentID();
            
            if(studentID.equalsIgnoreCase(stuID)&& !studentID.isEmpty())
            {   
                 return stuID;  
            }
        }
        return stuID;
    }
    
    //checks if the booking ID associates with the student ID
    public boolean authenticateBookingId(String studentID, int bID){
        boolean valid = true;
        ArrayList <Booking> bookingList = booking.getBookingList();
        
        if(bID>bookingList.size())
        {
            printAlertMessage("Booking ID does not exist");
            navigationPane();
        }
        
       
        for(Booking b: bookingList)
        {
            int booID = bookingList.get(bID-1).getBookingID();
            String stuID = bookingList.get(bID-1).getStudentID();
            
            if(booID == bID && stuID.equalsIgnoreCase(studentID) && !stuID.isEmpty())
            {
                valid = true;
            }
            else
            {   
                valid = false;
            }
        }
        return valid;
    }
    
    
    //checking availibilty for the same slot
    public int checkAvailableSlots(int exWeek, int exDay, int exSession, int exID)
    {
        int slotsAvailability = 0;
        
        ArrayList <Booking> bookingList = booking.getBookingList();
        for(Booking b: bookingList)
        {
            int bookingId = b.getBookingID();
            int exerciseWeek = b.getExerciseWeek();
            int exerciseDay = b.getExerciseDay();
            int exerciseSession = b.getExerciseSession();
            int exerciseID = b.getExerciseID();
            String bookingStatus = b.getBookingStatus();
            
            if(exID == exerciseID && exWeek == exerciseWeek && exDay == exerciseDay && exSession == exerciseSession && bookingStatus.equalsIgnoreCase(BOOKED) )
            {
                slotsAvailability++;
                
            }
            
        }
        return slotsAvailability;
    }
    
    //one student cannot attend more than 1 class on a given week,day and session
    public boolean checkDuplicateStudent(int exWeek, int exDay, int exSession, int exID ,String stuID)
    {
        ArrayList <Booking> bookingList = booking.getBookingList();
        for(Booking b: bookingList)
        {
            int exerciseWeek = b.getExerciseWeek();
            int exerciseDay = b.getExerciseDay();
            int exerciseSession = b.getExerciseSession();
            int exerciseID = b.getExerciseID();
            String studentID = b.getStudentID();
            String bookingStatus = b.getBookingStatus();
            

                if(exWeek == exerciseWeek && exDay == exerciseDay && exSession == exerciseSession && exID == exerciseID && studentID.equalsIgnoreCase(stuID)
                        && bookingStatus.equalsIgnoreCase(BOOKED))
                {
                return false;
                }

        }
        
        return true;
    }
    
    
    //booking a class method
    public void bookingSlots(){
        printDivider();
        int exDay =0;
        int exWeek =0;
        int exSession =0;
        int exID=0;
        do{
            System.out.println("Exercise classes available days : 0." +DAYS[0]+ " | 1." +DAYS[1]+"\n");
            System.out.print("Please enter your choice 0 or 1: ");
            exDay = input.nextInt();
        }while(exDay<0||exDay>1);
            printDivider();
        do{    
            System.out.println("Exercise class weeks : " +WEEKS[1]+ " | " +WEEKS[2]
                    +" | " +WEEKS[3]+" | " +WEEKS[4]+" | " +WEEKS[5]+" | " +WEEKS[6]+" | " +WEEKS[7]+" | " +WEEKS[8]+"\n");
            System.out.print("Please select a week (1 - 8): ");
            exWeek = input.nextInt();
        }while(exWeek<1||exWeek>8);
            printDivider();
        do{
            System.out.println("Available sessions per day : 0."+SESSIONS[0] +" | 1."+SESSIONS[1]+" | 2."+SESSIONS[2]+"\n");
            System.out.print("Please enter your choice 0 or 1 or 2: ");
            exSession = input.nextInt();
        }while(exSession<0||exSession>2);
            printDivider();
            listAllExercise(exWeek);
        do{
            System.out.println("0. "+ exList.get(0).getExerciseName()+ 
                                "\t1. "+ exList.get(1).getExerciseName()+
                                "\t2. "+ exList.get(2).getExerciseName()+
                                "\t3. "+ exList.get(3).getExerciseName()+
                                "\n\nPlease enter a choice(0-3): ");
            exID = input.nextInt();
        }while(exID<0||exID>3);
            printDivider();
            
            if (!(exWeek<1 && exWeek>8 && exDay<0  && exDay>1 && exSession <0 && exSession >2 && exID<0 && exID >3))
            {
                int slotsAvailability = checkAvailableSlots(exWeek,exDay,exSession,exID);
                int bookingID=1;
                if (slotsAvailability < MAX_STUDENTS)
                {
                        student.addStudentDetails();
                        stuDetails.add(stuDetails.size(),student);
                        
                        if (checkDuplicateStudent(exWeek, exDay, exSession, exID ,student.getStudentID()))
                        {
                            booking.createBooking(bookingID, exWeek,exDay, exSession, exID,student.getStudentID(),student.getStudentName(), slotsAvailability + 1, BOOKED,DISABLED,NOT_REVIEWED);
                            printHeader(student.toString());
                            System.out.println("You have succesfully booked : \t\t" + exList.get(exID).getExerciseName().toUpperCase()
                                    + "\nDay: \t\t\t\t\t"+DAYS[exDay]
                                    +"\nSession: \t\t\t\t"+SESSIONS[exSession]
                                    + "\nWeek: \t\t\t\t\t"+WEEKS[exWeek]
                                    + "\nBooking ID :\t\t\t\t"+ booking.getBookingList().size());
                            printDivider();
                            printDivider();
                        }
                        else
                        {
                            printDivider();
                            printAlertMessage("Unsuccessful !! Student already has an existing booking for this slot");
                            printDivider();
                        }
                }
                else
                {
                    printDivider();
                    printAlertMessage("<< FULLY BOOKED >>\n(This slot has reached its maximum occupancy of Students.) \nPlease choose some other options..");
                    printDivider();
                }   
                
            }
            else
            {
                printAlertMessage("Invalid input");
                navigationPane();
            }
    }
    
     public void modifiedBookingSlot(int bookingID, String bookingStatus){
       printDivider();
        int exDay =0;
        int exWeek =0;
        int exSession =0;
        int exID=0;
        do{
            
            System.out.println("Exercise classes available days : 0." +DAYS[0]+ " | 1." +DAYS[1]+"\n");
            System.out.print("Please enter your choice 0 or 1: ");
            exDay = input.nextInt();
        }while(exDay<0||exDay>1);
            printDivider();
        do{    
            System.out.println("Exercise class weeks : " +WEEKS[1]+ " | " +WEEKS[2]
                    +" | " +WEEKS[3]+" | " +WEEKS[4]+" | " +WEEKS[5]+" | " +WEEKS[6]+" | " +WEEKS[7]+" | " +WEEKS[8]+"\n");
            System.out.print("Please select a week (1 - 8): ");
            exWeek = input.nextInt();
        }while(exWeek<1||exWeek>8);
            printDivider();
        do{
            System.out.println("Available sessions per day : 0."+SESSIONS[0] +" | 1."+SESSIONS[1]+" | 2."+SESSIONS[2]+"\n");
            System.out.print("Please enter your choice 0 or 1 or 2: ");
            exSession = input.nextInt();
        }while(exSession<0||exSession>2);
            printDivider();
            listAllExercise(exWeek);
            printDivider();
        do{
            System.out.println("0. "+ exList.get(0).getExerciseName()+ 
                                "\t1. "+ exList.get(1).getExerciseName()+
                                "\t2. "+ exList.get(2).getExerciseName()+
                                "\t3. "+ exList.get(3).getExerciseName()+
                                "\n\nPlease enter your choice: ");
            exID = input.nextInt();
        }while(exID<0||exID>3);
            printDivider();
            
            if (!(exWeek<1 && exWeek>8 && exDay<0  && exDay>1 && exSession <0 && exSession >2 && exID<0 && exID >3))
            {
                int slotsAvailability = checkAvailableSlots(exWeek,exDay,exSession,exID);
                
                if (slotsAvailability < MAX_STUDENTS)
                {
                        
                        if (checkDuplicateStudent(exWeek, exDay, exSession, exID ,student.getStudentID()))
                        {
                            booking.changeBookings(bookingID, exWeek,exDay, exSession, exID,student.getStudentID(),student.getStudentName(), slotsAvailability + 1, bookingStatus,DISABLED,NOT_REVIEWED);
                       
                            printDivider();
                        }
                        else
                        {
                            printDivider();
                            printAlertMessage("Unsuccessful !! Student already has an existing booking for this slot");
                            printDivider();
                        }
                }
                else
                {
                    printDivider();
                    printAlertMessage("<< FULLY BOOKED >>\tThis slot has reached maximum occupancy of 4 Students \nPlease choose some other options..");
                    printDivider();
                }   
                
            }
            else
            {
                printAlertMessage("Invalid input");
                navigationPane();
            }
    
     }
     
    
    public String userReview(String rMessage)
    {
        String s = "Please try again";
//        boolean empty = rMessage.isEmpty();
        int size = rMessage.length();
        while(size>20)
            {
                return s;
            }
        return rMessage;
    }
    
 // checks if the booking is cancelled or already attended.
    public boolean authAttendanceStatus(int bID){
        ArrayList <Booking> bookingList = booking.getBookingList();
        String bStatus = bookingList.get(bID-1).getBookingStatus();
        
        if(bStatus.equalsIgnoreCase(BOOKED)||bStatus.equalsIgnoreCase(CHANGED))
        {
            return true;
        }
        return false;
        
    }
    
    // only change the bookings that are booked.
    public boolean authChangeStatus(int bID){
        ArrayList <Booking> bookingList = booking.getBookingList();
        String bStatus = bookingList.get(bID-1).getBookingStatus();
        
        if(bStatus.equalsIgnoreCase(BOOKED))
        {
            return true;
        }
        return false;  
    }
    
    // only cancel the bookings that are booked or changed.
    public boolean authCancelStatus(int bID){
        ArrayList <Booking> bookingList = booking.getBookingList();
        String bStatus = bookingList.get(bID-1).getBookingStatus();
        
        if(bStatus.equalsIgnoreCase(BOOKED)|| bStatus.equalsIgnoreCase(CHANGED))
        {
            return true;
        }
        return false;  
    }
    
    
    
    
    //print the highest grossing class revenue
    public void printChampionReport(int choice){
        
        ArrayList <Booking> bookingList = booking.getBookingList();
        
        if(choice == 1)
        {
            int yogaCount = 0; int boxingCount =0; int crossfitCount =0; int zumbaCount =0;
            double sumY =0;double sumB =0; double sumC =0; double sumZ =0;
            for(Booking b: bookingList)
            {
            int exID = b.getExerciseID();
            int exerciseWeek = b.getExerciseWeek();
            String bStatus = b.getBookingStatus();
            double exRevenueY = exList.get(0).getExercisePrice();
            double exRevenueB = exList.get(1).getExercisePrice();
            double exRevenueC = exList.get(2).getExercisePrice();
            double exRevenueZ = exList.get(3).getExercisePrice();
            
            //excludes cancelled reservations from total revenue
            if(exerciseWeek>0 && exerciseWeek<=4 && !bStatus.equalsIgnoreCase(CANCELLED))
            {
                if(exID == 0)
                { 
                    yogaCount++;
                    sumY= exRevenueY*yogaCount;
                }
                else if(exID==1)
                {
                    boxingCount++;
                    sumB= exRevenueB*boxingCount;
                }
                else if(exID==2)
                {
                    crossfitCount++;
                    sumC= exRevenueC*crossfitCount;
                }
                else if(exID==3)
                {
                    zumbaCount++;
                    sumZ= exRevenueZ*zumbaCount;
                }
            }
            }
            if(sumY>=sumB && sumY>=sumC && sumY>=sumZ)
            {
                reportBorder();
                System.out.printf("%n Class: Yoga\t\t Gross income: £%.2f %n",sumY);
                printDivider();
            }
            else if(sumB>=sumY && sumB>=sumC && sumB>=sumZ)
            {
                reportBorder();
                System.out.printf("%n Class: Boxing\t\t Gross income: £%.2f %n",sumB);
                printDivider();
            }
            else if(sumC>=sumY && sumC>=sumB && sumC>=sumZ)
            {
                reportBorder();
                System.out.printf("%n Class: Crossfit\t\t Gross income: £%.2f %n",sumC);
                printDivider();
            }
            else if(sumZ>=sumY && sumZ>=sumB && sumZ>=sumB)
            {
                reportBorder();
                System.out.printf("%n Class: Zumba\t\t Gross income: £%.2f %n",sumZ);
                printDivider();
            }
        }
        else if(choice == 2)
        {
            int yogaCount = 0; int boxingCount =0; int crossfitCount =0; int zumbaCount =0;
            double sumY =0;double sumB =0; double sumC =0; double sumZ =0;
            for(Booking b: bookingList)
            {
            int exID = b.getExerciseID();
            int exerciseWeek = b.getExerciseWeek();
            String bStatus = b.getBookingStatus();
            double exRevenueY = exList.get(0).getExercisePrice();
            double exRevenueB = exList.get(1).getExercisePrice();
            double exRevenueC = exList.get(2).getExercisePrice();
            double exRevenueZ = exList.get(3).getExercisePrice();
            
            //excludes cancelled reservations from total revenue
            if(exerciseWeek>4 && exerciseWeek<=8 && !bStatus.equalsIgnoreCase(CANCELLED))
            {
                if(exID == 0)
                { 
                    yogaCount++;
                    sumY= exRevenueY*yogaCount;
                }
                else if(exID==1)
                {
                    boxingCount++;
                    sumB= exRevenueB*boxingCount;
                }
                else if(exID==2)
                {
                    crossfitCount++;
                    sumC= exRevenueC*crossfitCount;
                }
                else if(exID==3)
                {
                    zumbaCount++;
                    sumZ= exRevenueZ*zumbaCount;
                }
            }
            }
            if(sumY>=sumB && sumY>=sumC && sumY>=sumZ)
            {
                reportBorder();
                System.out.printf("%n Class: Yoga\t\t Gross income: £%.2f %n",sumY);
                printDivider();
            }
            else if(sumB>=sumY && sumB>=sumC && sumB>=sumZ)
            {
                reportBorder();
                System.out.printf("%n Class: Boxing\t\t Gross income: £%.2f %n",sumB);
                printDivider();
            }
            else if(sumC>=sumY && sumC>=sumB && sumC>=sumZ)
            {
                reportBorder();
                System.out.printf("%n Class: Crossfit\t\t Gross income: £%.2f %n",sumC);
                printDivider();
            }
            else if(sumZ>=sumY && sumZ>=sumB && sumZ>=sumB)
            {
                reportBorder();
                System.out.printf("%n Class: Zumba\t\t Gross income: £%.2f %n",sumZ);
                printDivider();
            }
        }
            
        
        
        
        
    }
    
    public void firstMonthlyReport(){
        int yogaCount = 0; int boxingCount =0; int crossfitCount =0; int zumbaCount =0;
        float ratingY=0f; float ratingB=0f; float ratingC=0f; float ratingZ=0f;
        ArrayList <Booking> bookingList = booking.getBookingList();
        for(Booking b: bookingList)
        {
            int exerciseWeek = b.getExerciseWeek();
            int exID = b.getExerciseID();
            int rRating = b.getRating();
            
            if(exerciseWeek>0 && exerciseWeek<=4){
                if(exID == 0)
                { 
                    yogaCount++;
                    ratingY= ratingY+ rRating;
                }
                else if(exID==1)
                {
                    boxingCount++;
                    ratingB= ratingB+ rRating;
                }
                else if(exID==2)
                {
                    crossfitCount++;
                    ratingC= ratingC+ rRating;
                }
                else if(exID==3)
                {
                    zumbaCount++;
                    ratingZ= ratingZ+ rRating;
                }
            }
            
        }
        
        System.out.printf("=======================1st [MONTH REPORT]=============================\n");
        try {
            System.out.printf("YOGA     : \tTotal students: ["+yogaCount+"]\t Average rating: %.2f %n", (ratingY/yogaCount));  
        }
        catch (ArithmeticException e) {
            System.out.println("YOGA     : \tTotal students: ["+yogaCount+"]\t Average rating: "+ 0.0);
            
        }
        try {
            System.out.printf("BOXING   : \tTotal students: ["+boxingCount+"]\t Average rating: %.2f %n", (ratingB/boxingCount));  
        }
        catch (ArithmeticException e) {
            System.out.println("BOXING   : \tTotal students: ["+boxingCount+"]\t Average rating: "+ 0.0);
        }
        try {
            System.out.printf("CROSSFIT : \tTotal students: ["+crossfitCount+"]\t Average rating: %.2f %n", (ratingC/crossfitCount));
        }
        catch (ArithmeticException e) {
            System.out.println("CROSSFIT : \tTotal students: ["+crossfitCount+"]\t Average rating: "+ 0.0);
        }
        try {
            System.out.printf("ZUMBA    : \tTotal students: ["+zumbaCount+"]\t Average rating: %.2f %n", (ratingZ/zumbaCount));
        }
        catch (ArithmeticException e) {
            System.out.println("ZUMBA    : \tTotal students: ["+zumbaCount+"]\t Average rating: "+ 0.0);
        }
    }
    
    public void secondMonthlyReport(){
        int yogCount = 0; int boxCount =0; int crossCount =0; int zumbCount =0;
        float rateY=0f; float rateB=0f; float rateC=0f; float rateZ=0f;
        ArrayList <Booking> bookingList = booking.getBookingList();
        for(Booking b: bookingList)
        {
            int exerciseWeek = b.getExerciseWeek();
            int exID = b.getExerciseID();
            int revRating = b.getRating();
            
            if(exerciseWeek>4 && exerciseWeek<=8 ){
                if(exID == 0)
                { 
                    yogCount++;
                    rateY= rateY+ revRating;
                }
                else if(exID==1)
                {
                    boxCount++;
                    rateB= rateB+ revRating;
                }
                else if(exID==2)
                {
                    crossCount++;
                    rateC= rateC+ revRating;
                }
                else if(exID==3)
                {
                    zumbCount++;
                    rateZ= rateZ+ revRating;
                }
            }   
        }
        System.out.println("=======================2nd [MONTH REPORT]=============================\n");
        try {
            System.out.printf("YOGA     : \tTotal students: ["+yogCount+"]\t Average rating: %.2f %n", (rateY/yogCount));  
        }
        catch (ArithmeticException e) {
            System.out.println("YOGA     : \tTotal students: ["+yogCount+"]\t Average rating: "+ 0.0);
            
        }
        try {
            System.out.printf("BOXING   : \tTotal students: ["+boxCount+"]\t Average rating: %.2f %n", (rateB/boxCount));  
        }
        catch (ArithmeticException e) {
            System.out.println("BOXING   : \tTotal students: ["+boxCount+"]\t Average rating: "+ 0.0);
        }
        try {
            System.out.printf("CROSSFIT : \tTotal students: ["+crossCount+"]\t Average rating: %.2f %n", (rateC/crossCount));
        }
        catch (ArithmeticException e) {
            System.out.println("CROSSFIT : \tTotal students: ["+crossCount+"]\t Average rating: "+ 0.0);
        }
        try {
            System.out.printf("ZUMBA    : \tTotal students: ["+zumbCount+"]\t Average rating: %.2f %n", (rateZ/zumbCount));
        }
        catch (ArithmeticException e) {
            System.out.println("ZUMBA    : \tTotal students: ["+zumbCount+"]\t Average rating: "+ 0.0);
        }
    }

    
    public void printAlertMessage(String message){
        System.out.println(message);
    }
    
    public void printHeader(String message){
        System.out.println(message);
    }
        
    public void printDivider (){
        System.out.println("\n\n_____________________________________________________________________________________________");
    }
    
    public void printTableBorder(){
         System.out.println("====================================================");
    }
    
    public void reportBorder(){
        System.out.printf("===============1st [MONTH CHAMPION REPORT]===============\n");
    }
   
    public int checkInputValidation (int input, int minRange, int maxRange){
        
        int number = -1;
        int i = -1;
        try
        {
            number=input;
            if(number<=i)
            {
                System.out.println("Invalid input \n Enter a valid input: ");
                input = 0;
                input=new Scanner(System.in).nextInt();
                return checkInputValidation(input,minRange,maxRange);
            }
            else if (number <i || number>maxRange)
            {
                System.out.println("please enter a value between "+minRange+" and "+maxRange+" \nEnter a valid input: ");
                input = 0;
                input=new Scanner(System.in).nextInt();
                return checkInputValidation(input,minRange,maxRange);
            }
            return number;
        }
        catch(Exception e){
            input = 0;
            System.out.println("Please enter an Integer value \n Enter a valid input: ");
            input=new Scanner(System.in).nextInt();
            return checkInputValidation(input,minRange,maxRange);
        }
    }
    
}
