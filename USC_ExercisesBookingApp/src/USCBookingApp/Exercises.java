/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USCBookingApp;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
/**
 *
 * @author Baratharun
 */
public class Exercises {
    private Scanner input = new Scanner(System.in);
    private String exerciseName;
    private double exercisePrice;
    private int exerciseID;
     
    
    
    
    public Exercises(String eName,double ePrice, int eID)
    {
        this.setExerciseName(eName);
        this.setExercisePrice(ePrice);
        this.setExerciseID(eID);
    }
    
    public Exercises()
    {
        this.exerciseName = "";
        this.exercisePrice = 0.00;
        this.exerciseID= 0;
    
    }
    

    /**
     * @return the exerciseName
     */
    public String getExerciseName()
    {
        return exerciseName;
    }

    /**
     * @param exerciseName the exerciseName to set
     */
    public void setExerciseName(String newExerciseName)
    {
        this.exerciseName = newExerciseName;
    }

    /**
     * @return the exercisePrice
     */
    public double getExercisePrice()
    {
        return exercisePrice;
    }

    /**
     * @param exercisePrice the exercisePrice to set
     */
    public void setExercisePrice(double newExercisePrice)
    {
        this.exercisePrice = newExercisePrice;
    }

    /**
     * @return the exerciseID
     */
    public int getExerciseID()
    {
        return exerciseID;
    }

    /**
     * @param exerciseID the exerciseID to set
     */
    public void setExerciseID(int newExerciseID)
    {
        this.exerciseID = newExerciseID;
    }


}
