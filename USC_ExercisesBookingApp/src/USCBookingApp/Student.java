/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USCBookingApp;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Baratharun
 */
public class Student {
    private String studentName;
    private String studentID;
    private ArrayList <Student> stuDetails = new ArrayList <Student>();
    
    Scanner input = new Scanner(System.in);
    
    
    
    public Student(String sName,String sID){
        this.setStudentName(sName);
        this.setStudentID(sID);
    }
    public Student(){
        studentName = "";
        studentID = "";
    }
    
    // checks if the booking list is empty
    /**
     * @return the studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @param studentName the studentName to set
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    /**
     * @return the studentID
     */
    public String getStudentID() {
        return studentID;
    }

    /**
     * @param studentID the studentID to set
     */
    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    /**
     * @return the stuDetails
     */
    public ArrayList <Student> getStuDetails() {
        return stuDetails;
    }

    /**
     * @param stuDetails the stuDetails to set
     */
    public void setStuDetails(ArrayList <Student> stuDetails) {
        this.stuDetails = stuDetails;
    }
    
    public void addStudentDetails(){
        boolean validV = true;
        String inpName ="";
        String inpID="";
        
        do{
            System.out.print("Please enter student name: ");
            inpName = input.nextLine();
            setStudentName(inpName.toUpperCase());
        }while(inpName.isEmpty());
        while (validV){
            System.out.print("Please enter the 2 digits student ID number: ");
            inpID = input.nextLine();
            char[] ch = inpID.toCharArray();

            if (ch.length<=1 || ch.length>=3){
            System.out.println("Invalid ID number please try again");
            validV =true;
            }
            else{
            setStudentID(inpID);
            break;
            }
        }
        
    }
    
    @Override
     public String toString(){
        return String.format("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ "
                + "\nStudent name: %-10s\t ID number: %3s", this.studentName ,this.studentID 
                +"\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        
    }
}
