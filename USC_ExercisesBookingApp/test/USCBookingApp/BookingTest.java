/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USCBookingApp;

import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Baratharun
 */
public class BookingTest {
    
    public BookingTest() {
    }
    
    

    /**
     * Test of createBooking method, of class Booking.
     */
    @Test
    public void testCreateBooking() {
        System.out.println("createBooking");
        int bookingID = 0;
        int exerciseWeek = 0;
        int exerciseDay = 0;
        int exerciseSession = 0;
        int exerciseID = 0;
        String studentID = "";
        String studentName = "";
        int maxBooking = 0;
        String bookingStatus = "";
        int rating = 0;
        String rMessage = "";
        Booking instance = new Booking();
        instance.createBooking(bookingID, exerciseWeek, exerciseDay, exerciseSession, exerciseID, studentID, studentName, maxBooking, bookingStatus, rating, rMessage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of changeBookings method, of class Booking.
     */
    @Test
    public void testChangeBookings() {
        System.out.println("changeBookings");
        int bID = 0;
        int exWeek = 0;
        int exDay = 0;
        int exSess = 0;
        int exID = 0;
        String sID = "";
        String sName = "";
        int maxBook = 0;
        String bStatus = "";
        int rating = 0;
        String rMessage = "";
        Booking instance = new Booking();
        instance.changeBookings(bID, exWeek, exDay, exSess, exID, sID, sName, maxBook, bStatus, rating, rMessage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of cancelBookings method, of class Booking.
     */
    @Test
    public void testCancelBookings() {
        System.out.println("cancelBookings");
        int bID = 0;
        String bStatus = "";
        Booking instance = new Booking();
        instance.cancelBookings(bID, bStatus);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updateBookings method, of class Booking.
     */
    @Test
    public void testUpdateBookings() {
        System.out.println("updateBookings");
        int bookingID = 0;
        int exerciseWeek = 0;
        int exerciseDay = 0;
        int exerciseSession = 0;
        int exerciseID = 0;
        String studentID = "";
        String studentName = "";
        int maxBooking = 0;
        String bookingStatus = "";
        int rating = 0;
        String rMessage = "";
        Booking instance = new Booking();
        instance.updateBookings(bookingID, exerciseWeek, exerciseDay, exerciseSession, exerciseID, studentID, studentName, maxBooking, bookingStatus, rating, rMessage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of attendBookings method, of class Booking.
     */
    @Test
    public void testAttendBookings() {
        System.out.println("attendBookings");
        int bID = 0;
        String bStatus = "";
        int rating = 0;
        String rMessage = "";
        Booking instance = new Booking();
        instance.attendBookings(bID, bStatus, rating, rMessage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of emptyBookingList method, of class Booking.
     */
    @Test
    public void testEmptyBookingList() {
        System.out.println("emptyBookingList");
        Booking instance = new Booking();
        boolean expResult = false;
        boolean result = instance.emptyBookingList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBookingID method, of class Booking.
     */
    @Test
    public void testGetBookingID() {
        System.out.println("getBookingID");
        Booking instance = new Booking();
        int expResult = 0;
        int result = instance.getBookingID();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setBookingID method, of class Booking.
     */
    @Test
    public void testSetBookingID() {
        System.out.println("setBookingID");
        int bookingID = 0;
        Booking instance = new Booking();
        instance.setBookingID(bookingID);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExerciseDay method, of class Booking.
     */
    @Test
    public void testGetExerciseDay() {
        System.out.println("getExerciseDay");
        Booking instance = new Booking();
        int expResult = 0;
        int result = instance.getExerciseDay();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setExerciseDay method, of class Booking.
     */
    @Test
    public void testSetExerciseDay() {
        System.out.println("setExerciseDay");
        int exerciseDay = 0;
        Booking instance = new Booking();
        instance.setExerciseDay(exerciseDay);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExerciseSession method, of class Booking.
     */
    @Test
    public void testGetExerciseSession() {
        System.out.println("getExerciseSession");
        Booking instance = new Booking();
        int expResult = 0;
        int result = instance.getExerciseSession();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setExerciseSession method, of class Booking.
     */
    @Test
    public void testSetExerciseSession() {
        System.out.println("setExerciseSession");
        int exerciseSession = 0;
        Booking instance = new Booking();
        instance.setExerciseSession(exerciseSession);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExerciseID method, of class Booking.
     */
    @Test
    public void testGetExerciseID() {
        System.out.println("getExerciseID");
        Booking instance = new Booking();
        int expResult = 0;
        int result = instance.getExerciseID();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setExerciseID method, of class Booking.
     */
    @Test
    public void testSetExerciseID() {
        System.out.println("setExerciseID");
        int exerciseID = 0;
        Booking instance = new Booking();
        instance.setExerciseID(exerciseID);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBookingStatus method, of class Booking.
     */
    @Test
    public void testGetBookingStatus() {
        System.out.println("getBookingStatus");
        Booking instance = new Booking();
        String expResult = "";
        String result = instance.getBookingStatus();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setBookingStatus method, of class Booking.
     */
    @Test
    public void testSetBookingStatus() {
        System.out.println("setBookingStatus");
        String bookingStatus = "";
        Booking instance = new Booking();
        instance.setBookingStatus(bookingStatus);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBookingList method, of class Booking.
     */
    @Test
    public void testGetBookingList() {
        System.out.println("getBookingList");
        Booking instance = new Booking();
        ArrayList<Booking> expResult = null;
        ArrayList<Booking> result = instance.getBookingList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setBookingList method, of class Booking.
     */
    @Test
    public void testSetBookingList() {
        System.out.println("setBookingList");
        ArrayList<Booking> bookingList = null;
        Booking instance = new Booking();
        instance.setBookingList(bookingList);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStudentID method, of class Booking.
     */
    @Test
    public void testGetStudentID() {
        System.out.println("getStudentID");
        Booking instance = new Booking();
        String expResult = "";
        String result = instance.getStudentID();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setStudentID method, of class Booking.
     */
    @Test
    public void testSetStudentID() {
        System.out.println("setStudentID");
        String studentID = "";
        Booking instance = new Booking();
        instance.setStudentID(studentID);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMaxBooking method, of class Booking.
     */
    @Test
    public void testGetMaxBooking() {
        System.out.println("getMaxBooking");
        Booking instance = new Booking();
        int expResult = 0;
        int result = instance.getMaxBooking();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMaxBooking method, of class Booking.
     */
    @Test
    public void testSetMaxBooking() {
        System.out.println("setMaxBooking");
        int maxBooking = 0;
        Booking instance = new Booking();
        instance.setMaxBooking(maxBooking);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStudentName method, of class Booking.
     */
    @Test
    public void testGetStudentName() {
        System.out.println("getStudentName");
        Booking instance = new Booking();
        String expResult = "";
        String result = instance.getStudentName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setStudentName method, of class Booking.
     */
    @Test
    public void testSetStudentName() {
        System.out.println("setStudentName");
        String studentName = "";
        Booking instance = new Booking();
        instance.setStudentName(studentName);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExerciseWeek method, of class Booking.
     */
    @Test
    public void testGetExerciseWeek() {
        System.out.println("getExerciseWeek");
        Booking instance = new Booking();
        int expResult = 0;
        int result = instance.getExerciseWeek();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setExerciseWeek method, of class Booking.
     */
    @Test
    public void testSetExerciseWeek() {
        System.out.println("setExerciseWeek");
        int exerciseWeek = 0;
        Booking instance = new Booking();
        instance.setExerciseWeek(exerciseWeek);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRating method, of class Booking.
     */
    @Test
    public void testGetRating() {
        System.out.println("getRating");
        Booking instance = new Booking();
        int expResult = 0;
        int result = instance.getRating();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRating method, of class Booking.
     */
    @Test
    public void testSetRating() {
        System.out.println("setRating");
        int rating = 0;
        Booking instance = new Booking();
        instance.setRating(rating);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getReviewMessage method, of class Booking.
     */
    @Test
    public void testGetReviewMessage() {
        System.out.println("getReviewMessage");
        Booking instance = new Booking();
        String expResult = "";
        String result = instance.getReviewMessage();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setReviewMessage method, of class Booking.
     */
    @Test
    public void testSetReviewMessage() {
        System.out.println("setReviewMessage");
        String reviewMessage = "";
        Booking instance = new Booking();
        instance.setReviewMessage(reviewMessage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of listAllBookings method, of class Booking.
     */
    @Test
    public void testListAllBookings() {
        System.out.println("listAllBookings");
        Booking instance = new Booking();
        instance.listAllBookings();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
